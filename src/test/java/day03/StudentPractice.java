package day03;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class StudentPractice {

    @Test
    public void basicAuthentication() {
        Response response = RestAssured.given()
                .when()
                .get("https://jsonplaceholder.typicode.com/posts")
                .then().log().all()
                .assertThat().statusCode(200)
                .and().contentType(ContentType.JSON)
               .extract().response();

        System.out.println(response.jsonPath().getString("[6].title"));
        Assert.assertEquals(response.jsonPath().getString("[6].title"), "magnam facilis autem" );

    }
}
