package day02;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

public class MyPractice {

    @Test
    public void test1(){
        RestAssured.given().get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .prettyPeek();
        RestAssured.given().log().all().get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .then().log().all().assertThat().statusCode(200).contentType(ContentType.JSON);
    }
}
