package day02;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class Parameters {

    /**
     * Send a request to log in using the url as parameters for your username and password
     */
    @Test
    public void fullURLWithParams() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=majidsoda@gmail.com&password=Test1234")
                .then().assertThat().statusCode(200).and().contentType(ContentType.JSON);

    }

    /**
     * Log in using Map as query params
     */
    @Test
    public void logInWithMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", "majidsoda@gmail.com");
        map.put("password", "Test1234");

        RestAssured.given().log().all()
                .queryParams(map)
                .when().post("https://api.octoperf.com/public/users/login")
                .then()
                .log().all().contentType(ContentType.JSON).assertThat().statusCode(200);
    }

    /**
     * Log in using query parameters and verify status code
     */
    @Test
    public void logInWithQueryParams() {
        RestAssured.given().log().all()
                .queryParams("username", "majidsoda@gmail.com")
                .queryParams("password", "Test1234")
                .when().post("https://api.octoperf.com/public/users/login")
                .then()
                .statusCode(200);
    }
    @Test
    public void logInWithMultipleQueryParams() {
        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";
        RestAssured.given().log().all()
                .queryParams("username", "majidsoda@gmail.com", "password", "Test1234")
                .when().post(path)
                .then().log().body()
                .statusCode(200);
    }


}
