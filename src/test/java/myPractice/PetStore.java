package myPractice;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class PetStore {

   @Test
    public void addNewPet(){
       String path = "https://petstore.swagger.io/v2/pet";
       RestAssured.given()
               .log()
               .all()
               .when()
               .post(path)
               .then()
               .log()
               .all();
   }
}
