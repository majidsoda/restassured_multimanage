package day01;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class StudentPractice {

   @Test
   public void printPrettyPeek(){
       RestAssured.given()
               .when()
               .get("https://restful-booker.herokuapp.com/booking")
               .prettyPeek();

   }
   @Test
    public void printPrettyPrint(){
       RestAssured.given()
               .when()
               .get("https://restful-booker.herokuapp.com/booking")
               .prettyPrint();
   }
   @Test
    public void statusCode(){
       RestAssured.given()
               .when()
               .get("https://restful-booker.herokuapp.com/booking")
               .then()
               .assertThat().statusCode(200);
   }
    @Test
    public void printPrettyPeek2(){
        RestAssured.given()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .prettyPeek();

    }
    @Test
    public void printPrettyPrint2(){
        RestAssured.given()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .prettyPrint();
    }
    @Test
    public void statusCode2(){
        RestAssured.given()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .then()
                .assertThat().statusCode(200);
    }
    @Test
    public void verifyContentType(){
        RestAssured.given()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .then().assertThat().contentType(ContentType.JSON);
    }
    @Test
    public void verifyStatusCodeAndContentType(){
       RestAssured.given()
               .when().get("https://restful-booker.herokuapp.com/booking/51")
               .then()
               .assertThat().contentType(ContentType.JSON)
               .and()
               .statusCode(200);
    }
    @Test
    public void uriLog(){
        RestAssured.given()
                .log()
                .uri()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .then()
                .assertThat()
                .statusCode(200);

    }
    @Test
    public void bodyLog(){
        RestAssured.given()
                .log()
                .body()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .then()
                .assertThat()
                .statusCode(200);

    }
    @Test
    public void verifyResponseBodyLogs(){
        RestAssured.given()
                .log()
                .all()
                .when()
                .get("https://restful-booker.herokuapp.com/booking/51")
                .then()
                .log()
                .all();

    }
    // TASK verify the response body contains 86400 for expiresInSec from response body
    @Test
    public void verifyTimestamp(){
        Response response = RestAssured.given().when()
                .post("https://api.octoperf.com/public/users/login?username=majidsoda@gmail.com&password=Test1234")
                .then()
                .extract()
                .response();
        System.out.println(response.jsonPath().getString("expiresInSec"));
        Assert.assertEquals(response.jsonPath().getString("expiresInSec"), "86400");
    }

}
