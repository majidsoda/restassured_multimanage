package multiManage;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.IResultMap;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.apache.http.HttpStatus.SC_OK;

public class E2eProjects {

    Map<String, Object> map;
    Response response;

    @BeforeTest
    public String token() {
        RestAssured.baseURI = "https://api.octoperf.com/";
        String path = "/public/users/login";
        map = new HashMap<>();

        map.put("username", "majidsoda@gmail.com");
        map.put("password", "Test1234");

        return RestAssured.given().queryParams(map)
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .post(path).then().statusCode(SC_OK)
                .extract().body().jsonPath().get("token");

    }

    @Test
    public void getMemberDefault() {
        String path = "https://api.octoperf.com/workspaces/member-of";
        response = RestAssured.given().header("authorization", token())
                .when().get(path)
                .then()
                .extract().response();

        map = new HashMap<>();

        map.put("id", response.jsonPath().getString("[2].id"));
        map.put("userId", response.jsonPath().getString("[2].userId"));

        System.out.println(response.prettyPrint());
        Assert.assertEquals(response.jsonPath().getString("[2].name"), "Default");
        Assert.assertEquals(response.jsonPath().getString("[2].description"), "this is the new default");
        Assert.assertEquals(response.jsonPath().getString("[2].userId"), "ihRYPYcBTewEfIgJx6ZQ");
        Assert.assertEquals(response.jsonPath().getString("[2].id"), map.get("id"));

    }

    @Test(dependsOnMethods = "getMemberDefault")
    public void createProject() {

        String path = "/design/projects";
        System.out.println("id = " + map.get("id"));
        System.out.println("userId = " + map.get("userId"));
        String jsonPayload = "{\"id\":\"\",\"created\":\"2021-03-11T06:15:20.845Z\",\"lastModified\":\"2021-03-11T06:15:20.845Z\",\"userId\":\"" + map.get("userId") + "\",\"workspaceId\":\"" + map.get("id") + "\",\"name\":\"testing22\",\"description\":\"testing\",\"type\":\"DESIGN\",\"tags\":[]}";

        response = RestAssured.given().header("Authorization", token())
                .header("Content-type", "application/json")
                .and()
                .body(jsonPayload)
                .when()
                .post(path)
                .then().extract().response();

        System.out.println(response.prettyPrint());
        System.out.println(response.jsonPath().getString("name"));
        Assert.assertEquals(response.jsonPath().getString("name"), "testing22");
        Assert.assertEquals(response.jsonPath().getString("description"), "testing");

        // add a map with variables id call it projectId
        map.put("projectId", response.jsonPath().getString("id"));
        System.out.println("projectId = " + map.get("projectId"));

    }

    @Test(dependsOnMethods = {"getMemberDefault", "createProject"})
    public void updateProject() {
        String updatePayload = "{\"created\":1615443320845,\"description\":\"testing\",\"id\":\"" + map.get("projectId") + "\",\"lastModified\":1629860121757,\"name\":\"testing Soto\",\"tags\":[],\"type\":\"DESIGN\",\"userId\":\"" + map.get("userId") + "\",\"workspaceId\":\"" + map.get("id") + "\"}";
        String path = "/design/projects/" + map.get("projectId");

        response = RestAssured.given()
                .header("authorization", token())
                .header("Content-type", "application/json")
                .and()
                .body(updatePayload)
                .when()
                .put(path)
                .then()
                .extract()
                .response();

        System.out.println(response.prettyPrint());

    }

    @Test(dependsOnMethods = {"getMemberDefault", "createProject", "updateProject"})
    public void deleteProject() {
        String deletePath = "/design/projects/" + map.get("projectId");
        response = RestAssured.given()
                .log().all()
                .header("Authorization", token())
                .when()
                .delete(deletePath)
                .then()
                .extract()
                .response();
        Assert.assertEquals(response.statusCode(), 204);

    }

}
